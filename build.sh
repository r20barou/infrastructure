echo "Let's build our project !"

if [[ ! -d builded_files ]]
    then mkdir -p builded_files/{publishers,subscribers,api}
fi

cd publishers/cmd && go build -o ../../builded_files/publishers ./... && cd ../..
cd subscribers/cmd &&  go build -o ../../builded_files/subscribers ./... && cd ../..
cd api/cmd && go build -o ../../builded_files/api ./... &&  cp -R swagger ../../builded_files/api && cd ../.. 
cp config.yaml builded_files/

echo "And here we are..."