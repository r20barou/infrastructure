import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		intervalMeasure: {
			firstDate: "",
			secondDate: "",
			nature: "",
			measureCount: 0,
			measures: [],
		},
		measureAverages: {
			date: "",
			tempAverage: 0.0,
			windAverage: 0.0,
			pressAverage: 0.0,
		},
  },
  getters: {
    getIntervalMeasure: state => {
      return state.intervalMeasure;
    },
    getMeasureAverages: state => {
      return state.measureAverages;
    }
  },
  mutations: {
    setAverageDay(state, data) {
      this.state.measureAverages.date = data.date;
			this.state.measureAverages.tempAverage = data.tempAverage;
			this.state.measureAverages.windAverage = data.windAverage;
			this.state.measureAverages.pressAverage = data.pressAverage;
    },
    setIntervalMeasure(state, data) {
      this.state.intervalMeasure.firstDate = data.firstDate;
      this.state.intervalMeasure.secondDate = data.secondDate;
      this.state.intervalMeasure.nature = data.nature;
      this.state.intervalMeasure.measureCount = data.measureCount;
      this.state.intervalMeasure.measures = data.measures;
    }
  },
	actions: {},
	modules: {},
});
