import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../components/home/Home.vue";
import Search from "../components/search/Search.vue";
import AvancedSearch from "../components/avancedSearch/AvancedSearch.vue";
import Results from "../components/results/Results.vue"

Vue.use(VueRouter);

const routes = [
	{
		path: "/",
		name: "Home",
		component: Home,
	},
	{
		path: "/search",
		name: "Search",
		component: Search,
	},
	{
		path: "/avanced_search",
		name: "Avanced Search",
		component: AvancedSearch,
	},
	{
		path: "/results/:request",
		name: "Results",
		component: Results,
		props: true
	}
	// {
	//   path: "/about",
	//   name: "About",
	//   // route level code-splitting
	//   // this generates a separate chunk (about.[hash].js) for this route
	//   // which is lazy-loaded when the route is visited.
	//   component: () =>
	//     import(/* webpackChunkName: "about" */ "../views/About.vue"),
	// },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
