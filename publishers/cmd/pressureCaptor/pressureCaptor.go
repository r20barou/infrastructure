package main

import (
	"publishers/internal/config"
	"publishers/internal/devMod"
	"publishers/internal/operator"
)

func main() {
	captorConfig := config.ReadCaptorConfig("pressCaptor")
	id, iata := devMod.GetMod(captorConfig.DevMod)
	operator.StartCapture(id, iata, "press", captorConfig)
}
