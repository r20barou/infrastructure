package mqtt

import (
	"encoding/json"
	"fmt"
	"log"
	"publishers/internal/config"
	"publishers/internal/measureStruct"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

func SetupClient(config config.MqttConfig) mqtt.Client {
	host := config.Host
	port := config.Port
	brokerURL := "tcp://" + host + ":" + port
	opts := mqtt.NewClientOptions()
	opts.AddBroker(brokerURL)
	opts.SetClientID("ID_Go_mqtt_client")
	client := mqtt.NewClient(opts)
	token := client.Connect()
	for !token.WaitTimeout(3 * time.Second) {
		if err := token.Error(); err != nil {
			log.Fatal(err)
		}
	}
	return client
}

func SendData(client mqtt.Client, data measureStruct.Measure) {
	values, err := json.Marshal(data)
	if err != nil {
		fmt.Println("Err: ", err)
	}
	client.Publish("world/airport/meteo_measures", byte(config.ReadMqttConfig().QoSLevel), false, values)
}
