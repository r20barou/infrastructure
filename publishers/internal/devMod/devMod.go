package devMod

import (
	"bufio"
	"flag"
	"fmt"
	"os"
)

func GetMod(devMod bool) (string, string) {
	var id string
	var iata string
	if devMod {
		id_arg := flag.String("id", "", "")
		iata_arg := flag.String("iata", "", "")
		flag.Parse()
		id = string(*id_arg)
		iata = string(*iata_arg)
	} else {
		fmt.Print("insert captorID: ")
		input := bufio.NewScanner(os.Stdin)
		input.Scan()
		id = input.Text()
		fmt.Print("insert IATA: ")
		input = bufio.NewScanner(os.Stdin)
		input.Scan()
		iata = input.Text()
	}
	if id == "" {
		id = "No_ID"
	}
	if iata == "" {
		iata = "No_IATA"
	}

	return id, iata
}
