package operator

import (
	"fmt"
	"math/rand"
	"publishers/internal/config"
	"publishers/internal/measureStruct"
	"publishers/internal/mqtt"
	"time"
)

var location, _ = time.LoadLocation("Europe/Paris")

func StartCapture(id string, iata string, natureMeasure string, captorConfig config.CaptorConfig) {
	fmt.Printf("Launch of a wind captor, id: %v airport : %v ...\n", id, iata)
	mqttConfig := config.ReadMqttConfig()
	client := mqtt.SetupClient(mqttConfig)
	for {
		measure := getMeasure(id, iata, natureMeasure, captorConfig)
		fmt.Printf("%+v\n", measure)
		mqtt.SendData(client, measure)
		time.Sleep(time.Duration(time.Duration(captorConfig.CaptureInterval) * time.Second))
	}
}

func getMeasure(id string, iata string, natureMeasure string, captorConfig config.CaptorConfig) measureStruct.Measure {
	rand.Seed(time.Now().UnixNano()) // nécéssaire pour que les valeurs aléatoires correspondent à la configuration
	var measure measureStruct.Measure
	measure.CaptorID = id
	measure.IATAcode = iata
	measure.NatureMeasure = natureMeasure
	measure.Value = captorConfig.MinValue + rand.Intn(captorConfig.MaxValue-captorConfig.MinValue)
	measure.Timestamp = time.Now().In(location).Unix()
	return measure
}
