package config

type CaptorConfig struct {
	CaptureInterval int  `json:"captureInterval"`
	MinValue        int  `json:"minValue"`
	MaxValue        int  `json:"maxValue"`
	DevMod          bool `json:"devMod"`
}

func NewCaptorConfig(interval int, minVal int, maxVal int, devMod bool) CaptorConfig {
	var config CaptorConfig
	config.CaptureInterval = interval
	config.MinValue = minVal
	config.MaxValue = maxVal
	config.DevMod = devMod
	return config
}

type MqttConfig struct {
	Host     string `json:"host"`
	Port     string `json:"port"`
	QoSLevel int    `json:"qosLevel"`
}

func NewMqttConfig(host string, port string, qos int) MqttConfig {
	var config MqttConfig
	config.Host = host
	config.Port = port
	config.QoSLevel = qos
	return config
}
