package config

import (
	"fmt"
	"os"

	"github.com/spf13/viper"
)

func setupReader() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("../")      // go run depuis publishers ou lancement du .exe depuis le répo d'executables
	viper.AddConfigPath("../..")    // go run depuis cmd
	viper.AddConfigPath("../../..") // go run depuis un répertoire capteur
	viper.AutomaticEnv()
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println("fatal error config file: default \n", err)
		os.Exit(1)
	}
}

func ReadCaptorConfig(captorType string) CaptorConfig {

	setupReader()

	interval := viper.GetInt("app.captors." + captorType + ".intervalCapture")
	minVal := viper.GetInt("app.captors." + captorType + ".minValue")
	maxVal := viper.GetInt("app.captors." + captorType + ".maxValue")
	devMod := viper.GetBool("app.captors." + captorType + ".devMod")
	return NewCaptorConfig(interval, minVal, maxVal, devMod)
}

func ReadMqttConfig() MqttConfig {
	setupReader()
	host := viper.GetString("app.mqtt.host")
	port := viper.GetString("app.mqtt.port")
	qos := viper.GetInt("app.mqtt.QoSLevel")
	return NewMqttConfig(host, port, qos)
}
