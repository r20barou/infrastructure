package measureStruct

type Measure struct {
	CaptorID      string `json:"id"`
	IATAcode      string `json:"iata"`
	NatureMeasure string `json:"nature"`
	Value         int    `json:"value"`
	Timestamp     int64  `json:"timestamp"`
}
