package timeParser

import (
	"log"
	"strconv"
	"time"
)

var dateFormat = "2006-01-02-15-04-05"
var location, _ = time.LoadLocation("Europe/Paris")

func DateToTimestamp(date string) int64 {
	t, err := time.Parse(dateFormat, date)
	if err != nil {
		log.Fatal(err)
	}
	dateUTC := time.Now().In(location)
	_, offset := dateUTC.Zone()
	valueWithUTC := t.Unix() - int64(offset) //(gestion UTC)
	return valueWithUTC
}

func TimestampToDate(timestamp string) string {
	date, err := strconv.ParseInt(timestamp, 10, 64)
	if err != nil {
		log.Fatal(err)
	}
	tm := time.Unix(date, 0).In(location).Format(dateFormat)
	return tm
}
