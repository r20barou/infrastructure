package db

import (
	"api/internal/config"
	"api/internal/entities"
	"log"
	"math/rand"
	"strconv"
	"strings"
	"time"

	dm "api/internal/dataManipulator"
	tp "api/internal/timeParser"

	"github.com/gomodule/redigo/redis"
)

var (
	dateFormat  = "2006-01-02-15-04-05"
	conn        redis.Conn
	db_adress   = config.ReadDbConfig().Host + ":" + config.ReadDbConfig().Port
	location, _ = time.LoadLocation("Europe/Paris")
)

func GetAverageDay(date string) entities.MeasureAverages {

	conn, _ = redis.Dial("tcp", db_adress)
	defer conn.Close()

	keys := getAllKeys("", "")

	firstDate := tp.DateToTimestamp(date)

	secondDate := firstDate + 86400 //secondes dans une journée

	r, err := conn.Do("ZRANGEBYSCORE", "measureTimeSet", firstDate, secondDate)
	checkError(err)
	sub_keys, err := redis.Strings(r, err)
	checkError(err)

	var windMeasures []string
	var tempMeasures []string
	var pressureMeasures []string

	for _, key := range keys {
		for _, sub_key := range sub_keys {
			reply, _ := conn.Do("HGET", key, sub_key)
			val, _ := redis.String(reply, err)
			if val != "" {
				if strings.Contains(key, "wind") {
					windMeasures = append(windMeasures, val)
				} else if strings.Contains(key, "temp") {
					tempMeasures = append(tempMeasures, val)
				} else if strings.Contains(key, "press") {
					pressureMeasures = append(pressureMeasures, val)
				}
			}
		}
	}

	windAverage := dm.GetAverage(dm.ConvertToNumerical(windMeasures))
	tempAverage := dm.GetAverage(dm.ConvertToNumerical(tempMeasures))
	pressAverage := dm.GetAverage(dm.ConvertToNumerical(pressureMeasures))

	var averages = entities.NewMeasuresAverages(
		date,
		tempAverage,
		windAverage,
		pressAverage)

	return averages
}

func GetIntervalMeasure(nature string, firstDate string, secondDate string) entities.IntervalMeasure {

	conn, _ = redis.Dial("tcp", db_adress)
	defer conn.Close()

	firstDate = strconv.Itoa(int(tp.DateToTimestamp(firstDate)))
	secondDate = strconv.Itoa(int(tp.DateToTimestamp(secondDate)))

	keys := getAllKeys("", nature)

	var measures []entities.DateValuePair

	r, err := conn.Do("ZRANGEBYSCORE", "measureTimeSet", firstDate, secondDate)
	checkError(err)
	sub_keys, err := redis.Strings(r, err)

	for _, key := range keys {
		for _, sub_key := range sub_keys {
			reply, _ := conn.Do("HGET", key, sub_key)
			valStr, _ := redis.String(reply, err)
			time, _ := conn.Do("ZSCORE", "measureTimeSet", sub_key)
			timestampVal, _ := redis.String(time, err)
			timeVal := tp.TimestampToDate(timestampVal)
			if valStr != "" {
				val, _ := strconv.Atoi(valStr)
				var value = entities.NewDateValuePair(timeVal, val)
				measures = append(measures, value)
			}
		}
	}

	var result = entities.NewIntervalMeasure(
		tp.TimestampToDate(firstDate),
		tp.TimestampToDate(secondDate),
		nature,
		len(measures),
		measures)
	return result
}

func GenerateStub(date string) {

	conn, _ = redis.Dial("tcp", db_adress)
	defer conn.Close()

	_, _ = conn.Do("FLUSHALL")
	rand.Seed(time.Now().UnixNano())
	t, _ := time.Parse(dateFormat, date)
	timestamp := t.In(location).Unix()

	for i := 0; i < 10; i++ {
		localTimestampWind := timestamp
		localTimeStampTemp := timestamp
		localTimeStampPressure := timestamp
		for j := 0; j < 50; j++ {
			idmeasure := strconv.FormatInt(int64(i), 10) + "-LYS-" + strconv.FormatInt(int64(j), 10)
			conn.Do("HSET", "captorID:"+strconv.FormatInt(int64(i), 10)+":iata:LYS:natureMeasure:wind", idmeasure, rand.Intn(50))
			conn.Do("ZADD", "measureTimeSet", localTimestampWind, idmeasure)
			localTimestampWind += 3600
		}
		for j := 0; j < 50; j++ {
			idmeasure := strconv.FormatInt(int64(i), 10) + "-LYS-" + strconv.FormatInt(int64(j), 10)
			conn.Do("HSET", "captorID:"+strconv.FormatInt(int64(i), 10)+":iata:LYS:natureMeasure:press", idmeasure, rand.Intn(50))
			conn.Do("ZADD", "measureTimeSet", localTimeStampPressure, idmeasure)
			localTimeStampPressure += 3600
		}
		for j := 0; j < 50; j++ {
			idmeasure := strconv.FormatInt(int64(i), 10) + "-LYS-" + strconv.FormatInt(int64(j), 10)
			conn.Do("HSET", "captorID:"+strconv.FormatInt(int64(i), 10)+":iata:LYS:natureMeasure:temperature", idmeasure, rand.Intn(50))
			conn.Do("ZADD", "measureTimeSet", localTimeStampTemp, idmeasure)
			localTimeStampTemp += 3600
		}
	}

}

func getAllKeys(iata string, nature string) []string {

	if iata == "" {
		iata = "*"
	}
	if nature == "" {
		nature = "*"
	}

	query := "captorID:*:iata:" + iata + ":natureMeasure:" + nature

	r, err := conn.Do("KEYS", query)
	checkError(err)
	keys, err := redis.Strings(r, err)
	checkError(err)
	return keys
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
		return
	}
}
