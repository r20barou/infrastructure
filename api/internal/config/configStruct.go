package config

type DbConfig struct {
	Host string `json:"host"`
	Port string `json:"port"`
}

func NewDbConfig(host string, port string) DbConfig {
	return DbConfig{Host: host, Port: port}
}

type ApiConfig struct {
	Host        string `json:"host"`
	Port        string `json:"port"`
	SwaggerPath string `json:"swaggerPath"`
}

func NewApiConfig(host string, port string, path string) ApiConfig {
	return ApiConfig{Host: host, Port: port, SwaggerPath: path}
}

type CaptorConfig struct {
	CaptureInterval int  `json:"captureInterval"`
	MinValue        int  `json:"minValue"`
	MaxValue        int  `json:"maxValue"`
	DevMod          bool `json:"devMod"`
}

func NewCaptorConfig(interval int, minVal int, maxVal int, devMod bool) CaptorConfig {
	var config CaptorConfig
	config.CaptureInterval = interval
	config.MinValue = minVal
	config.MaxValue = maxVal
	config.DevMod = devMod
	return config
}

type MqttConfig struct {
	Host string `json:"host"`
	Port string `json:"port"`
}

func NewMqttConfig(host string, port string) MqttConfig {
	var config MqttConfig
	config.Host = host
	config.Port = port
	return config
}
