package config

import (
	"fmt"
	"os"

	"github.com/spf13/viper"
)

func setupReader() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("../")   // go run depuis publishers ou lancement du .exe depuis le répo d'executables
	viper.AddConfigPath("../..") // go run depuis cmd
	viper.AddConfigPath("../../..")
	viper.AutomaticEnv()
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println("fatal error config file: default \n", err)
		os.Exit(1)
	}
}

func ReadDbConfig() DbConfig {

	setupReader()

	host := viper.GetString("app.redis.host")
	port := viper.GetString("app.redis.port")

	return NewDbConfig(host, port)
}

func ReadAPIConfig() ApiConfig {

	setupReader()

	host := viper.GetString("app.api.host")
	port := viper.GetString("app.api.port")
	swaggerPath := viper.GetString("app.api.swaggerPath")

	return NewApiConfig(host, port, swaggerPath)
}
