package controller

import (
	"api/internal/db"
	"net/http"

	gin "github.com/gin-gonic/gin"
)

func GenerateStub(c *gin.Context) {
	date := c.Param("date")
	db.GenerateStub(date)
	c.IndentedJSON(http.StatusOK, "Data has been generated")
}

func GetAverageDay(c *gin.Context) {

	date := c.Param("measureTime")
	averages := db.GetAverageDay(date)
	c.IndentedJSON(http.StatusOK, averages)
}

func GetIntervalMeasure(c *gin.Context) {

	nature := c.Param("nature")
	firstDate := c.Param("firstDate")
	secondDate := c.Param("secondDate")

	measures := db.GetIntervalMeasure(nature, firstDate, secondDate)

	c.IndentedJSON(http.StatusOK, measures)
}
