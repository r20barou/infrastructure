package entities

type IntervalMeasure struct {
	FirstDate     string          `json:"firstDate"`
	SecondDate    string          `json:"secondDate"`
	Nature        string          `json:"nature"`
	MeasuresCount int             `json:"measureCount"`
	Measures      []DateValuePair `json:"measures"`
}

func NewIntervalMeasure(f_date string, s_date string, nature string, measuresCount int, measures []DateValuePair) IntervalMeasure {
	var measure IntervalMeasure
	measure.FirstDate = f_date
	measure.SecondDate = s_date
	measure.Nature = nature
	measure.MeasuresCount = measuresCount
	measure.Measures = measures
	return measure
}
