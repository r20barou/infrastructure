package entities

type DateValuePair struct {
	Date  string `json:"date"`
	Value int    `json:"value"`
}

func NewDateValuePair(date string, value int) DateValuePair {
	var dvp DateValuePair
	dvp.Date = date
	dvp.Value = value
	return dvp
}
