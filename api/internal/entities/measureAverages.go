package entities

type MeasureAverages struct {
	Date         string  `json:"date"`
	TempAverage  float64 `json:"tempAverage"`
	WindAverage  float64 `json:"windAverage"`
	PressAverage float64 `json:"pressAverage"`
}

func NewMeasuresAverages(date string, tempAverage float64, windAverage float64, pressAverage float64) MeasureAverages {
	var ma MeasureAverages
	ma.Date = date
	ma.TempAverage = tempAverage
	ma.PressAverage = pressAverage
	ma.WindAverage = windAverage
	return ma
}
