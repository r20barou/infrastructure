package dataManipulator

import (
	"log"
	"strconv"
)

func GetAverage(measures []int) float64 {
	sum := 0
	n := len(measures)
	if n == 0 {
		return -1
	}
	for i := 0; i < n; i++ {
		sum += measures[i]
	}
	return (float64(sum) / (float64(n)))
}

func ConvertToNumerical(measures []string) []int {
	var numericalMeasures []int
	for _, measure := range measures {
		numericalMeasure, err := strconv.Atoi(measure)
		if err != nil {
			log.Fatal(err)
		}
		numericalMeasures = append(numericalMeasures, numericalMeasure)
	}
	return numericalMeasures
}
