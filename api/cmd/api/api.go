package main

import (
	"api/internal/config"
	"api/internal/controller"

	"github.com/gin-contrib/cors"
	gin "github.com/gin-gonic/gin"
)

func main() {

	router := gin.Default()

	host := config.ReadAPIConfig().Host
	port := config.ReadAPIConfig().Port

	router.Use(cors.New(cors.Config{
		AllowAllOrigins: true,
	}))

	v1 := router.Group("/api/v1")
	{
		v1.GET("/getAverageDay/:measureTime", controller.GetAverageDay)
		v1.GET("/getIntervalMeasure/:nature/:firstDate/:secondDate", controller.GetIntervalMeasure)
		v1.GET("/generateStub/:date", controller.GenerateStub)
	}
	router.Static("/swagger/", "./swagger") // il faut go run depuis cmd

	router.Run(host + ":" + port)
}
