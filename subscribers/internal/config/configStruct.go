package config

type MqttConfig struct {
	Host     string `json:"host"`
	Port     string `json:"port"`
	QoSLevel int    `json:"qosLevel"`
}

func NewMqttConfig(host string, port string, qos int) MqttConfig {
	var config MqttConfig
	config.Host = host
	config.Port = port
	config.QoSLevel = qos
	return config
}

type DbConfig struct {
	Host string `json:"host"`
	Port string `json:"port"`
}

func NewDbConfig(host string, port string) DbConfig {
	return DbConfig{Host: host, Port: port}
}
