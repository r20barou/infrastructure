package config

import (
	"fmt"
	"os"

	"github.com/spf13/viper"
)

func setupReader() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("../")      // go run depuis publishers ou lancement du .exe depuis le répo d'executables
	viper.AddConfigPath("../..")    // go run depuis cmd
	viper.AddConfigPath("../../..") // go run depuis un répertoire capteur
	viper.AutomaticEnv()
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println("fatal error config file: default \n", err)
		os.Exit(1)
	}
}

func ReadDbConfig() DbConfig {

	setupReader()

	host := viper.GetString("app.redis.host")
	port := viper.GetString("app.redis.port")

	return NewDbConfig(host, port)
}

func ReadMqttConfig() MqttConfig {
	setupReader()
	host := viper.GetString("app.mqtt.host")
	port := viper.GetString("app.mqtt.port")
	qos := viper.GetInt("app.mqtt.QoSLevel")
	return NewMqttConfig(host, port, qos)
}
