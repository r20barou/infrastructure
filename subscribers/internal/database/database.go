package database

import (
	"encoding/json"
	"fmt"

	"github.com/gomodule/redigo/redis"

	"subscriberDB/internal/config"
	"subscriberDB/internal/subStruct"
	"subscriberDB/internal/tools"
)

func saveDataInRedis(conn redis.Conn, data subStruct.Data) {
	var idmeasure string

	conn.Do("HINCRBY", "captorID:"+data.CaptorID+
		":iata:"+data.Iata+
		":natureMeasure:"+data.NatureMeasure, "incr", 1)

	r, _ := redis.String(conn.Do("HGET", "captorID:"+data.CaptorID+
		":iata:"+data.Iata+
		":natureMeasure:"+data.NatureMeasure, "incr"))

	idmeasure = data.CaptorID + "-" + data.Iata + "-" + r

	conn.Do("HSET", "captorID:"+data.CaptorID+
		":iata:"+data.Iata+
		":natureMeasure:"+data.NatureMeasure, idmeasure, data.Value)

	conn.Do("ZADD", "measureTimeSet", data.Timestamp, idmeasure)
}

func displayMeasureTimeSet(conn redis.Conn) {
	result, err := redis.Strings(conn.Do("ZRANGE", "measureTimeSet", 0, -1))
	tools.CheckError(err)
	for _, item := range result {
		fmt.Println(item)
	}
}

func ManageData(msg []byte) {
	var data subStruct.Data

	conn, err := redis.Dial("tcp", config.ReadDbConfig().Host+":"+config.ReadDbConfig().Port) //connect to the redis server
	tools.CheckError(err)
	defer conn.Close()
	_ = json.Unmarshal(msg, &data) //json data turns into struct
	saveDataInRedis(conn, data)
	displayMeasureTimeSet(conn)
}
