package timeParser

import (
	"time"
)

var location, _ = time.LoadLocation("Europe/Paris")

//var dateFormat = "2006-01-02"

func TimeStampToDate(timestamp int64, dateFormat string) string {
	tm := time.Unix(timestamp, 0).In(location).Format(dateFormat)
	return tm
}
