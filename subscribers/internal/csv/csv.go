package csv

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strconv"
	files "subscriberDB/internal/filesFoldersManager"
	"subscriberDB/internal/subStruct"
	"subscriberDB/internal/timeParser"
)

var appdata, _ = os.UserConfigDir()

func ManageData(msg []byte) {
	var data subStruct.Data

	_ = json.Unmarshal(msg, &data) //json data turns into struct
	fmt.Println(data)
	WriteCSV(data)
}

func WriteCSV(data subStruct.Data) {
	path := appdata + "\\measuresCSV\\" + data.Iata + "\\" + data.NatureMeasure
	file := files.GetFile(path, data.Timestamp)
	w := csv.NewWriter(file)
	row := []string{timeParser.TimeStampToDate(data.Timestamp, "15-04-05"), data.CaptorID, strconv.Itoa(data.Value)}
	if err := w.Write(row); err != nil {
		log.Fatalln("error writing measure to file", err)
	}
	w.Flush()
	file.Close()
}
