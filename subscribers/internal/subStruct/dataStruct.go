package subStruct

type Data struct {
	CaptorID      string `json:"id"`
	Iata          string `json:"iata"`
	NatureMeasure string `json:"nature"`
	Value         int    `json:"value"`
	Timestamp     int64  `json:"timestamp"`
}
