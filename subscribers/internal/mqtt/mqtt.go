package mqtt

import (
	"fmt"
	"log"
	"time"

	"subscriberDB/internal/config"
	"subscriberDB/internal/csv"
	"subscriberDB/internal/database"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

var topic string = "world/airport/meteo_measures"

var messageSubHandlerDB mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	database.ManageData(msg.Payload())
}

var messageSubHandlerCSV mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	csv.ManageData(msg.Payload())
}

func createClientOptions(brokerURI string, clientId string) *mqtt.ClientOptions {
	opts := mqtt.NewClientOptions()

	opts.AddBroker(brokerURI)
	opts.SetClientID(clientId)
	return opts
}

func Connect(brokerURI string, clientId string) mqtt.Client {
	fmt.Println("Trying to connect (" + brokerURI + ", " + clientId + ")...")
	opts := createClientOptions(brokerURI, clientId)
	if clientId == "subDB" {
		opts.OnConnect = func(client mqtt.Client) {
			client.Subscribe(topic, byte(config.ReadMqttConfig().QoSLevel), messageSubHandlerDB)
		}
	} else if clientId == "subCSV" {
		opts.OnConnect = func(client mqtt.Client) {
			client.Subscribe(topic, byte(config.ReadMqttConfig().QoSLevel), messageSubHandlerCSV)
		}
	}
	client := mqtt.NewClient(opts)
	token := client.Connect()
	for !token.WaitTimeout(3 * time.Second) {
		if err := token.Error(); err != nil {
			log.Fatal(err)
		} else {
			fmt.Println("Connected to server")
		}
	}
	return client
}
