package filesFoldersManager

import (
	"encoding/csv"
	"log"
	"os"
	"subscriberDB/internal/timeParser"
)

func GetFile(path string, timestamp int64) *os.File {
	if !Exists(path) {
		os.MkdirAll(path, os.ModePerm)
	}
	var file *os.File
	var err error
	path = path + "\\" + timeParser.TimeStampToDate(timestamp, "2006-01-02") + ".csv"
	if !Exists(path) {
		file, err = os.Create(path)
		w := csv.NewWriter(file)
		row := []string{"heures-minutes-secondes", "ID capteur", "valeur"}
		if err := w.Write(row); err != nil {
			log.Fatalln("error writing measure to file", err)
		}
		w.Flush()
	} else {
		file, err = os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	}
	if err != nil {
		log.Fatalln("failed to open file", err)
	}
	return file
}

func Exists(path string) bool {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return false
	}
	return true
}
