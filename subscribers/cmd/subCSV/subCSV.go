package main

import (
	"os"
	"os/signal"
	files "subscriberDB/internal/filesFoldersManager"
	"subscriberDB/internal/mqtt"
	"syscall"
)

func main() {
	appdata, _ := os.UserConfigDir()
	if !files.Exists(appdata + "/measuresCSV") {
		os.MkdirAll(appdata+"/measuresCSV", 0755)
	}
	run()
}

func run() {
	channel := make(chan os.Signal, 1)
	signal.Notify(channel, os.Interrupt, syscall.SIGTERM)
	client := mqtt.Connect("tcp://localhost:1883", "subCSV")
	token := client.Connect()
	if token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	<-channel
}
