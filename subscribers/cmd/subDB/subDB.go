package main

import (
	"os"
	"os/signal"
	"syscall"

	"subscriberDB/internal/config"
	"subscriberDB/internal/mqtt"
)

func main() {
	run()
}

func run() {
	brokerURL := "tcp://" + config.ReadMqttConfig().Host + ":" + config.ReadMqttConfig().Port
	channel := make(chan os.Signal, 1)
	signal.Notify(channel, os.Interrupt, syscall.SIGTERM)
	client := mqtt.Connect(brokerURL, "subDB")
	token := client.Connect()
	if token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	<-channel
}
