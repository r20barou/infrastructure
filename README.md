# ! English Below !

# Projet infrastructure IMT Atlantique 2A 
Cynthia ANDRIAMPARIVONY, Victoire LENGLART,Martin KEYLING, Romain BAROU, 
IMT Atlantique FIL A2, Promotion 2023

# Lancer le projet:

## Une fois le broker MQTT et le serveur redis lancés depuis la racine du projet:

### Lancer un publisher:
#### Depuis le répértoire cmd du sous répértoire publishers:
Capteur pression : ` go run ./pressureCaptor/pressureCaptor.go `
Capteur température: `go run ./tempCaptor/tempCaptor.go`
Capteur vent: ` go run ./windCaptor/windCaptor.go `
### Lancer un subscriber: 
#### Depuis le répértoire cmd du sous répértoire subscribers:
Subscriber pour Redis: ` go run ./subDB/subDB.go `
Subscriber pour les csv: ` go run ./subCSV/subCSV.go `
Les csv sont disponibles dans %appdata%/measuresCSV
### Lancer l'API: 
#### Depuis le répértoire cmd du sous répértoire API:
Lancer  ` go run ./api/api.go `
L'API est hebergée sur ` http://localhost:8080/ `
La documentation est disponible sur ` http://localhost:8080/swagger/ `
### Lancer L'IHM: 
#### Depuis le répértoire IHM\\infrastructure-ihm:
Initialiser les node modules: ` npm install `
Lancer l'IHM: ` npm run serve `
### Générer tous les éxécutables:
#### Depuis la racine du projet:
`./build.sh`
Les éxécutables sont disponibles dans le sous répértoire `builded_files`

# English Version

# infrastructure's project IMT Atlantique 2A 
Cynthia ANDRIAMPARIVONY, Victoire LENGLART,Martin KEYLING, Romain BAROU, 
IMT Atlantique FIL A2, Promotion 2023

# Start the project:

## When the MQTT broker and the server are launched, from the project's root:

### start a publisher:
#### from the cmd repository from the publishers sub repository:
Pressure captor : ` go run ./pressureCaptor/pressureCaptor.go `
temperature captor : `go run ./tempCaptor/tempCaptor.go`
Wind captor : ` go run ./windCaptor/windCaptor.go `

### start a subscriber: 
#### from the cmdr repository from subscribers sub repository:
Subscriber for Redis: ` go run ./subDB/subDB.go `
Subscriber for csv: ` go run ./subCSV/subCSV.go `
csv files are avaible here: %appdata%/measuresCSV

### Start the API: 
#### from the cmd repository from the API sub-repository:
start  ` go run ./api/api.go `
L'API est hosted here ` http://localhost:8080/ `
the documentation is avaible here ` http://localhost:8080/swagger/ `

### Start the HMI: 
#### from the repository IHM\\infrastructure-ihm:
Initiate node modules: ` npm install `
Start HMI : ` npm run serve `

### Generate .exe files:
#### From the project's root:
`./build.sh`
.exe are avaible in the file : `builded_files`
